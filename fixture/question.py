# Sample questions used in unit tests and illustrative example.
# DISCLAIMER: the quizzes content is reproduced with the prior consent of Sandbox Networks, Inc.

from model.question import Question

QUESTION_LIST1 = [
    Question("What do the Simpsons use to escape the dome?",
             choices=[
                 ('A', 'motorcycle'),
                 ('B', 'helicopter'),
                 ('C', 'sinkhole'),
                 ('D', 'tank')
             ],
             correct_choices=['A', 'B']
             ),
    Question("Who does Lisa develop a crush on?",
             choices=[
                 ('A', 'Colin'),
                 ('B', 'Mihouse'),
                 ('C', 'Harry'),
                 ('D', 'Tom')
             ],
             correct_choices=['C']
             ),
    Question("What does Marge save from the Simpsons' burning house?",
             choices=[
                 ('A', 'photos'),
                 ('B', 'wedding video'),
                 ('C', 'Santas Little Helper'),
                 ('D', 'dishes')
             ],
             correct_choices=['B', 'D']
             )
]

QUESTION_LIST2 = [
    Question("Which of these characters is not in Homer's B-Sharps band?",
             choices=[
                 ('A', 'Moe'),
                 ('B', 'Barney'),
                 ('C', 'Principal Skinner'),
                 ('D', 'Apu')
             ],
             correct_choices=['C']
             ),
    Question("What singer alternates lyrics to his song when singing to Homer in space with Buzz Aldrin?",
             choices=[
                 ('A', 'Paul Simon'),
                 ('B', 'James Taylor'),
                 ('C', 'Eric Clapton'),
                 ('D', 'Elton John')
             ],
             correct_choices=['A', 'C']
             )
]