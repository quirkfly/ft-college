#!/usr/bin/python3

# College Demo Implementation
# Illustrates usage and collaboration of model classes.

from model.semester import Semester, Period
from model.teacher import Teacher
from model.clazz import Class
from model.student import Student
from model.quizz import Quizz
from fixture.question import QUESTION_LIST1, QUESTION_LIST2

def main():
    semester = Semester(Period.WINTER, 2018)
    teacher = Teacher(Teacher.make_id(), "Louis", "Dunn")
    
    clazz = Class('I.A', semester, teacher)
    student1 = Student(Student.make_id(), "John", "Sanchez")
    student2 = Student(Student.make_id(), "Kayla", "Blanchette")
    clazz.add_students([student1, student2])    
    teacher.add_class(clazz)
    
    quizz1 = teacher.create_quizz(id=Quizz.make_id(), name="The Simpsons Quiz for Beginners",
                                  questions=QUESTION_LIST1)    
    teacher.assign_quizz(quizz1, clazz.name)
    
    student1.answer_question(quizz_id=quizz1.id, question_text=QUESTION_LIST1[0].text, choices=['A', 'B'])
    student1.answer_question(quizz_id=quizz1.id, question_text=QUESTION_LIST1[1].text, choices=['C'])
    student1.answer_question(quizz_id=quizz1.id, question_text=QUESTION_LIST1[2].text, choices=['B', 'D'])
    student2.answer_question(quizz_id=quizz1.id, question_text=QUESTION_LIST1[0].text, choices=['A', 'B'])
    student2.answer_question(quizz_id=quizz1.id, question_text=QUESTION_LIST1[1].text, choices=['A'])
    
    quizz2 = teacher.create_quizz(id=Quizz.make_id(), name="The Simpsons Quiz for Experts",
                                  questions=QUESTION_LIST2)
    teacher.assign_quizz(quizz2, clazz.name)
    
    student1.answer_question(quizz_id=quizz2.id, question_text=QUESTION_LIST2[0].text, choices=['C'])
    student1.answer_question(quizz_id=quizz2.id, question_text=QUESTION_LIST2[1].text, choices=['A', 'C'])
    student2.answer_question(quizz_id=quizz2.id, question_text=QUESTION_LIST2[0].text, choices=['C'])
    
    teacher.evaluate_classes()

    for student in [student1, student2]:
        print(student)


if __name__ == '__main__':
    main()