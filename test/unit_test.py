#!/usr/bin/python3

# unit test implementation

import unittest

from fixture.question import QUESTION_LIST1, QUESTION_LIST2
from model.exception import ClassAlreadyExists, NoSuchClass, StudentAlreadyExist, NoSuchStudent, NoSuchQuestion
from model.clazz import Class
from model.quizz import Quizz
from model.semester import Semester, Period
from model.student import Student
from model.teacher import Teacher


class TeacherTestCase(unittest.TestCase):
    """Tests for Teacher class."""

    def setUp(self):
        self.question_list1 = QUESTION_LIST1
        self.question_list2 = QUESTION_LIST2


    def test_equality(self):
        teacher1 = Teacher(Teacher.make_id(), "Louis", "Dunn")
        teacher2 = Teacher(teacher1.id, "Louis", "Dunn")
        self.assertEqual(teacher1, teacher2, "teachers are identical")


    def test_inequality(self):
        teacher1 = Teacher(Teacher.make_id(), "Louis", "Dunn")
        teacher2 = Teacher(Teacher.make_id(), "Jason", "Baird")
        self.assertNotEqual(teacher1, teacher2, "teachers are not identical")


    def test_add_class(self):
        semester = Semester(Period.WINTER, 2018)
        teacher = Teacher(Teacher.make_id(), "Louis", "Dunn")
        clazz = Class('I.A', semester, teacher)
        teacher.add_class(clazz)
        self.assertEqual(teacher.number_classes, 1, "teacher has got 1 class")

        with self.assertRaises(ClassAlreadyExists):
            teacher.add_class(clazz)


    def test_add_classes(self):
        semester = Semester(Period.WINTER, 2018)
        teacher = Teacher(Teacher.make_id(), "Louis", "Dunn")
        clazz1 = Class('II.A', semester, teacher)
        clazz2 = Class('I.B', semester, teacher)
        teacher.add_classes([clazz1, clazz2])
        self.assertEqual(teacher.number_classes, 2, "teacher has got 2 classes")

        with self.assertRaises(ClassAlreadyExists):
            teacher.add_classes([clazz1, clazz2])


    def test_remove_class(self):
        semester = Semester(Period.WINTER, 2018)
        teacher = Teacher(Teacher.make_id(), "Louis", "Dunn")
        clazz = Class('I.A', semester, teacher)
        teacher.add_class(clazz)
        self.assertEqual(teacher.number_classes, 1, "teacher has got 1 class")

        teacher.remove_class(clazz.name)
        self.assertEqual(teacher.number_classes, 0, "teacher has got no class")

        with self.assertRaises(NoSuchClass):
            teacher.remove_class(clazz.name)


    def test_contains_class(self):
        semester = Semester(Period.WINTER, 2018)
        teacher = Teacher(Teacher.make_id(), "Louis", "Dunn")
        clazz1 = Class('I.A', semester, teacher)
        teacher.add_class(clazz1)
        self.assertTrue(clazz1 in teacher, "teacher teaches class")

        clazz2 = Class('II.A', semester, teacher)
        self.assertFalse(clazz2 in teacher, "teacher does not teach class")


    def test_create_quizz(self):
        teacher = Teacher(Teacher.make_id(), "Louis", "Dunn")
        quizz_id = Quizz.make_id()
        quizz_name = "The Simpsons Quiz for Beginners"
        quizz = teacher.create_quizz(id=quizz_id, name=quizz_name, questions=self.question_list1)
        self.assertIsInstance(quizz, Quizz, "quizz was created")
        self.assertEqual(quizz.id, quizz_id, "quizz id is initialized")
        self.assertEqual(quizz.name, quizz_name, "quizz name is initialized")
        self.assertEqual(quizz.number_questions, 3, "quizz consists of 3 questions")
        self.assertEqual(quizz.get_question(1), self.question_list1[0], "quizz contains first question")
        self.assertEqual(quizz.get_question(2), self.question_list1[1], "quizz contains second question")
        self.assertEqual(quizz.get_question(3), self.question_list1[2], "quizz contains third question")


    def test_assign_quizz(self):
        semester = Semester(Period.WINTER, 2018)
        teacher = Teacher(Teacher.make_id(), "Louis", "Dunn")

        clazz = Class('I.A', semester, teacher)
        student1 = Student(Student.make_id(), "John", "Sanchez")
        student2 = Student(Student.make_id(), "Kayla", "Blanchette")
        clazz.add_students([student1, student2])
        self.assertEqual(clazz.size, 2, "class consists of 2 students")

        teacher.add_class(clazz)
        self.assertTrue(clazz in teacher, "teacher teaches class")

        quizz = teacher.create_quizz(id=Quizz.make_id(), name="The Simpsons Quiz for Beginners",
                                     questions=self.question_list1)
        self.assertIsInstance(quizz, Quizz, "quizz was created")

        teacher.assign_quizz(quizz, clazz.name)
        self.assertEqual(len(clazz.quizzes), 1, "class is assigned 1 quizz")


    def test_evaluate_clases(self):
        semester = Semester(Period.WINTER, 2018)
        teacher = Teacher(Teacher.make_id(), "Louis", "Dunn")

        clazz = Class('I.A', semester, teacher)
        student1 = Student(Student.make_id(), "John", "Sanchez")
        student2 = Student(Student.make_id(), "Kayla", "Blanchette")
        clazz.add_students([student1, student2])
        self.assertEqual(clazz.size, 2, "class consists of 2 students")

        teacher.add_class(clazz)
        self.assertTrue(clazz in teacher, "teacher teaches class")

        quizz1 = teacher.create_quizz(id=Quizz.make_id(), name="The Simpsons Quiz for Beginners",
                                      questions=self.question_list1)
        self.assertIsInstance(quizz1, Quizz, "quizz1 was created")

        teacher.assign_quizz(quizz1, clazz.name)

        student1.answer_question(quizz_id=quizz1.id, question_text=self.question_list1[0].text, choices=['A', 'B'])
        student1.answer_question(quizz_id=quizz1.id, question_text=self.question_list1[1].text, choices=['C'])
        student1.answer_question(quizz_id=quizz1.id, question_text=self.question_list1[2].text, choices=['B', 'D'])
        student2.answer_question(quizz_id=quizz1.id, question_text=self.question_list1[0].text, choices=['A', 'B'])
        student2.answer_question(quizz_id=quizz1.id, question_text=self.question_list1[1].text, choices=['A'])

        quizz2 = teacher.create_quizz(id=Quizz.make_id(), name="The Simpsons Quiz for Experts",
                                      questions=self.question_list2)
        self.assertIsInstance(quizz2, Quizz, "quizz2 was created")

        teacher.assign_quizz(quizz2, clazz.name)

        student1.answer_question(quizz_id=quizz2.id, question_text=self.question_list2[0].text, choices=['C'])
        student1.answer_question(quizz_id=quizz2.id, question_text=self.question_list2[1].text, choices=['A', 'C'])
        student2.answer_question(quizz_id=quizz2.id, question_text=self.question_list2[0].text, choices=['C'])

        student_grades = teacher.evaluate_classes()
        self.assertIsInstance(student_grades, list, "student_grades is list")
        self.assertEqual(student_grades[0], (student1.id, 'A'), "student1 got 'A'")
        self.assertEqual(student_grades[1], (student2.id, 'C'), "student1 got 'C'")


class StudentTestCase(unittest.TestCase):
    """Tests for Student class."""

    def setUp(self):
        self.question_list1 = QUESTION_LIST1
        self.question_list2 = QUESTION_LIST2


    def test_equality(self):
        student1 = Student(Student.make_id(), "John", "Sanchez")
        student2 = Student(student1.id, "John", "Sanchez")
        self.assertEqual(student1, student2, "students are identical")


    def test_inequality(self):
        student1 = Student(Student.make_id(), "John", "Sanchez")
        student2 = Student(Student.make_id(), "Kayla", "Blanchette")
        self.assertNotEqual(student1, student2, "students are not identical")


    def test_answer_question(self):
        student = Student(Student.make_id(), "John", "Sanchez")
        quizz = Quizz(id=Quizz.make_id(), name="The Simpsons Quiz for Beginners", questions=self.question_list1)

        for question in quizz:
            student.answer_question(quizz.id, question.text, ['A', 'B'])
            self.assertEqual(student.answers[quizz.id][question.text], ['A', 'B'])


    def test_submit_answers(self):
        student = Student(Student.make_id(), "John", "Sanchez")
        quizz = Quizz(id=Quizz.make_id(), name="The Simpsons Quiz for Beginners", questions=self.question_list1)

        for question in quizz:
            student.answer_question(quizz.id, question.text, ['A', 'B'])
            self.assertEqual(student.answers[quizz.id][question.text], ['A', 'B'])

        answers = student.submit_answers(quizz.id)
        self.assertEqual(len(answers), len(self.question_list1), "all anwsers submitted")
        for i, question_text in enumerate(iter(answers.keys())):
            self.assertEqual(self.question_list1[i].text, question_text, "question texts are identical")



class ClassTestCase(unittest.TestCase):
    """Tests for Class class."""

    def setUp(self):
        self.question_list1 = QUESTION_LIST1


    def test_equality(self):
        class1 = Class('I.A', None, None)
        class2 = Class('I.A', None, None)
        self.assertEqual(class1, class2, "classes are identical")


    def test_inequality(self):
        class1 = Class('I.A', None, None)
        class2 = Class('II.A', None, None)
        self.assertNotEqual(class1, class2, "classes are not identical")


    def test_add_student(self):
        clazz = Class('I.A', None, None)
        student = Student(Student.make_id(), "John", "Sanchez")
        clazz.add_student(student)
        self.assertEqual(clazz.size, 1, "class consists of 1 student")

        with self.assertRaises(StudentAlreadyExist):
            clazz.add_student(student)


    def test_add_students(self):
        clazz = Class('I.A', None, None)
        student1 = Student(Student.make_id(), "John", "Sanchez")
        student2 = Student(Student.make_id(), "Kayla", "Blanchette")
        clazz.add_students([student1, student2])
        self.assertEqual(clazz.size, 2, "class consists of 2 students")

        with self.assertRaises(StudentAlreadyExist):
            clazz.add_students([student1, student2])


    def test_remove_student(self):
        clazz = Class('I.A', None, None)
        student = Student(Student.make_id(), "John", "Sanchez")
        clazz.add_student(student)
        self.assertEqual(clazz.size, 1, "class consists of 1 student")

        clazz.remove_student(student.id)
        self.assertEqual(clazz.size, 0, "class is empty")

        with self.assertRaises(NoSuchStudent):
            clazz.remove_student(student.id)


    def test_remove_students(self):
        clazz = Class('I.A', None, None)
        student1 = Student(Student.make_id(), "John", "Sanchez")
        student2 = Student(Student.make_id(), "Kayla", "Blanchette")
        clazz.add_students([student1, student2])
        self.assertEqual(clazz.size, 2, "class consists of 2 students")

        clazz.remove_students([student1.id, student2.id])
        self.assertEqual(clazz.size, 0, "class is empty")

        with self.assertRaises(NoSuchStudent):
            clazz.remove_students([student1.id, student2.id])


    def test_contains_student(self):
        clazz = Class('I.A', None, None)
        student1 = Student(Student.make_id(), "John", "Sanchez")
        clazz.add_student(student1)
        self.assertTrue(student1 in clazz, "class contains student")

        student2 = Student(Student.make_id(), "Kayla", "Blanchette")
        self.assertFalse(student2 in clazz, "class does not contain student")


    def test_get_student(self):
        clazz = Class('I.A', None, None)
        student = Student(Student.make_id(), "John", "Sanchez")
        clazz.add_student(student)
        self.assertEqual(clazz.get_student(1), student, "students are identical")

        with self.assertRaises(NoSuchStudent):
            clazz.get_student(2)


    def test_assign_quizz(self):
        clazz = Class('I.A', None, None)
        quizz = Quizz(id=Quizz.make_id(), name="The Simpsons Quiz for Beginners", questions=self.question_list1)
        clazz.assign_quizz(quizz)
        self.assertEqual(len(clazz.quizzes), 1, "class is assigned 1 quizz")


    def test_evaluate_students(self):
        teacher = Teacher(Teacher.make_id(), "Louis", "Dunn")

        clazz = Class('I.A', None, teacher)
        student1 = Student(Student.make_id(), "John", "Sanchez")
        student2 = Student(Student.make_id(), "Kayla", "Blanchette")
        clazz.add_students([student1, student2])
        self.assertEqual(clazz.size, 2, "class consists of 2 students")

        teacher.add_class(clazz)
        self.assertTrue(clazz in teacher, "teacher teaches class")

        quizz1 = teacher.create_quizz(id=Quizz.make_id(), name="The Simpsons Quiz for Beginners",
                                      questions=self.question_list1)
        self.assertIsInstance(quizz1, Quizz, "quizz1 was created")

        teacher.assign_quizz(quizz1, clazz.name)

        student1.answer_question(quizz_id=quizz1.id, question_text=self.question_list1[0].text, choices=['A', 'B'])
        student1.answer_question(quizz_id=quizz1.id, question_text=self.question_list1[1].text, choices=['C'])
        student1.answer_question(quizz_id=quizz1.id, question_text=self.question_list1[2].text, choices=['B', 'D'])
        student2.answer_question(quizz_id=quizz1.id, question_text=self.question_list1[0].text, choices=['A', 'B'])
        student2.answer_question(quizz_id=quizz1.id, question_text=self.question_list1[1].text, choices=['A'])

        expected_success_rates = dict()
        expected_success_rates[student1.id] = 1.0
        expected_success_rates[student2.id] = 0.3333333333333333

        student_evaluation = clazz.evaluate_students()
        self.assertIsInstance(student_evaluation, dict, "student_evaluation is dictionary")
        for student_id, quizz_evaluations in iter(student_evaluation.items()):
            self.assertEqual(expected_success_rates[student_id], quizz_evaluations[0]['success_rate'],
                             "success rates are equal")



class QuizzTestCase(unittest.TestCase):
    """Tests for Quizz class."""

    def setUp(self):
        self.question_list1 = QUESTION_LIST1
        self.question_list2 = QUESTION_LIST2


    def test_equality(self):
        quizz1 = Quizz(id=Quizz.make_id(), name="The Simpsons Quiz for Beginners", questions=self.question_list1)
        quizz2 = Quizz(quizz1.id, name="The Simpsons Quiz for Beginners", questions=self.question_list1)
        self.assertEqual(quizz1, quizz2, "quizzes are identical")


    def test_inequality(self):
        quizz1 = Quizz(id=Quizz.make_id(), name="The Simpsons Quiz for Beginners", questions=self.question_list1)
        quizz2 = Quizz(id=Quizz.make_id(), name="The Simpsons Quiz for Experts", questions=self.question_list2)
        self.assertNotEqual(quizz1, quizz2, "quizzes are not identical")


    def test_number_questions(self):
        quizz = Quizz(id=Quizz.make_id(), name="The Simpsons Quiz for Beginners", questions=self.question_list1)
        self.assertEqual(quizz.number_questions, 3, "quizz consists of 3 questions")


    def test_get_question(self):
        quizz = Quizz(id=Quizz.make_id(), name="The Simpsons Quiz for Beginners", questions=self.question_list1)
        question = quizz.get_question(1)
        self.assertEqual(question, self.question_list1[0], "questions are identical")

        with self.assertRaises(NoSuchQuestion):
            quizz.get_question(len(self.question_list1) + 1)



class SemesterTestCase(unittest.TestCase):
    """Tests for Semester class."""

    def test_equality(self):
        semester1 = Semester(Period.SUMMER, 2018)
        semester2 = Semester(Period.SUMMER, 2018)
        self.assertEqual(semester1, semester2, "semesters are identical")


    def test_inequality(self):
        semester1 = Semester(Period.SUMMER, 2018)
        semester2 = Semester(Period.WINTER, 2018)
        self.assertNotEqual(semester1, semester2, "semesters are not identical")



class QuestionTestCase(unittest.TestCase):
    """Tests for Question class."""

    def setUp(self):
        self.questions = QUESTION_LIST1


    def test_equality(self):
        self.assertEqual(self.questions[0], self.questions[0], "questions are identical")


    def test_inequality(self):
        self.assertNotEqual(self.questions[0], self.questions[1], "questions are not identical")


    def test_contains_choice(self):
        self.assertTrue('A' in self.questions[0], "question contains choice 'A'")
        self.assertFalse('X' in self.questions[0], "question does not contain choice 'X'")


    def test_are_choices_correct(self):
        selected_choices = self.questions[0].correct_choices
        self.assertTrue(self.questions[0].are_choices_correct(selected_choices) , "choices are correct")


if __name__ == '__main__':
    unittest.main()