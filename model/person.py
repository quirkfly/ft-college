# Model of person. Person represents a base class of living beings working in / attending a college.
# It knows how to create an id for new instance of a class derived from it. Implements comparison operator.

from uuid import uuid4

class Person(object):
    @classmethod
    def make_id(cls):
        return str(uuid4())


    def __init__(self, id, first_name, last_name):
        self.id = id
        self.first_name = first_name
        self.last_name = last_name


    def __eq__(self, other):
        """Returns True if this and other person are equal, False otherwise."""

        return self.id == other.id


    def __repr__(self):
        """Returns this person representation."""

        return '<Person: id = "{}", first_name = "{}", last_name ="{}">'.format(self.id, self.first_name,
                                                                                self.last_name)