# Model of teacher. Each teacher aggregates and manages his/her classes.
# Moreover teacher knows how to create a quizz and assign it to a specific class.
# Finally teacher evaluates all assigned classes.

from model.exception import ClassAlreadyExists, NoSuchClass
from model.person import Person
from model.quizz import Quizz


def get_grade(success_rate, num_quizzes):
    """Returns grade based on success rate and number of quizzes.
    >>> get_grade(2.0, 2)
    'A'
    >>> get_grade(0.8333333333333333, 2)
    'C'
    """

    if success_rate >= num_quizzes - 0.5:
        return 'A'
    elif success_rate >= num_quizzes - 1:
        return 'B'
    elif success_rate >= num_quizzes - 1.5:
        return 'C'
    elif success_rate >= num_quizzes - 2.5:
        return 'D'
    else:
        return 'E'


class Teacher(Person):
    def __init__(self, id, first_name, last_name, classes=None):
        super().__init__(id, first_name, last_name)

        self.classes = { clazz.name: clazz for clazz in classes } if classes is not None else {}


    @property
    def number_classes(self):
        """Returns number of classes this teacher teaches."""

        return len(self.classes)


    def add_class(self, clazz):
        """Adds a class to the list of classes this teacher teaches if not there already,
        raises ClassAlreadyExist exception otherwise."""

        if clazz.name in self.classes:
            raise ClassAlreadyExists

        self.classes[clazz.name] = clazz


    def add_classes(self, classes):
        """Adds list of classes to the lists of classes this teacher teaches. Raises ClassAlreadyExist exception
        if there is already such class in the list of classes."""

        for clazz in classes:
            self.add_class(clazz)


    def remove_class(self, class_name):
        """Removes a class with corresponding name if in the list of classes this teacher teaches,
        raises NoSuchClass exception otherwise."""

        if not class_name in self.classes:
            raise NoSuchClass

        del self.classes[class_name]


    def __contains__(self, clazz):
        """Returns True if this teacher teaches a class, False otherwise."""

        return clazz in list(self.classes.values())


    def create_quizz(self, id, name, questions):
        """Creates and returns a new quizz."""

        return Quizz(id, name, questions)


    def assign_quizz(self, quizz, class_name):
        """Assigns a quizz to a class with corresponding name. Raises NoSuchClass if class was not found."""

        if not class_name in self.classes:
            raise NoSuchClass

        self.classes[class_name].assign_quizz(quizz)


    def evaluate_classes(self):
        """Evaluates all classes. Returns list of (student_id, grade) pairs."""

        student_grades = []

        for clazz in list(self.classes.values()):
            for student_id, quizz_evaluations in iter(clazz.evaluate_students().items()):
                class_success_rate = 0
                for quizz_evaluation in quizz_evaluations:
                    class_success_rate += quizz_evaluation['success_rate']
                grade = get_grade(class_success_rate, len(clazz.quizzes))
                student = clazz.students[student_id]
                student.set_grade(clazz.semester.label, grade)

                student_grades.append((student.id, grade))

        return student_grades


    def grade_students(self):
        """Grades stundents. Returns """


    def __repr__(self):
        """Returns teacher string representation."""

        return '<Teacher: id = "{}", first_name = "{}", last_name ="{}", classes = {}>'.format(self.id,
                                                                                               self.first_name,
                                                                                               self.last_name,
                                                                                               self.classes)


if __name__ == "__main__":
    import doctest
    doctest.testmod()
