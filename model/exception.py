# Implementation of exceptions raised by model classes.

class ClassAlreadyExists(Exception):
    pass


class NoSuchClass(Exception):
    pass


class StudentAlreadyExist(Exception):
    pass


class NoSuchStudent(Exception):
    pass


class QuizzAlreadyExists(Exception):
    pass


class NoSuchQuizz(Exception):
    pass


class NoSuchQuestion(Exception):
    pass


class NoSuchChoice(Exception):
    pass


class NoTeacherForQuizz(Exception):
    pass


class SemesterAlreadyExists(Exception):
    pass