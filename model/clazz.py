# Model of class. Each class maintains a mapping between student id and student instance.
# It implements student iterator.
# In addition the class manages a list of quizzes that are assign to it by a teacher.
# Finally class participates in the student evaluation process.

from model.exception import StudentAlreadyExist, NoSuchStudent, QuizzAlreadyExists


class Class(object):
    def __init__(self, name, semester, teacher, students=None):
        self.name = name
        self.semester = semester
        self.teacher = teacher
        self.students = { student.id: student for student in students } if students is not None else {}
        self.student_index = 0
        self.quizzes = []


    @property
    def size(self):
        """Returns number of students in this class."""

        return len(self.students)


    def __eq__(self, other):
        """Returns True if this and other class are equal, False otherwise."""

        return self.name == other.name


    def add_student(self, student):
        """Adds a student to this class if not there already, raises StudentAlreadyExist exception otherwise."""

        if student.id in self.students:
            raise StudentAlreadyExist

        self.students[student.id] = student


    def add_students(self, students):
        """Adds list of students to this class. Raises StudentAlreadyExist exception
        if there is already such student in the class."""

        for student in students:
            self.add_student(student)


    def remove_student(self, student_id):
        """Removes a student with id from this class if there, raises NoSuchStudent exception otherwise."""

        if not student_id in self.students:
            raise NoSuchStudent

        del self.students[student_id]


    def remove_students(self, student_ids):
        """Removes list of student with ids from this class if there, raises NoSuchStudent exception otherwise."""

        for student_id in student_ids:
            self.remove_student(student_id)


    def __contains__(self, student):
        """Returns True if this class contains a student, False otherwise."""

        return student.id in self.students


    def get_student(self, index):
        """Returns student at <index> (1 referring to first student) position if within range,
        raises IndexError exception otherwise."""

        students = list(self.students.values())

        try:
            return students[index - 1]
        except Exception:
            raise NoSuchStudent


    def __iter__(self):
        """Initializes student iterator context and returns self."""

        self.student_index = 0

        return self


    def __next__(self):
        """Returns next student if within range, raises StopIteration otherwise."""

        if self.student_index < len(self.students):
            student = self.get_student(self.student_index + 1)
            self.student_index += 1

            return student
        else:
            raise StopIteration


    def assign_quizz(self, quizz):
        """Assigns a quizz to this class. Raises QuizzAlreadyExists if assigned already."""

        if quizz in self.quizzes:
            raise QuizzAlreadyExists

        self.quizzes.append(quizz)


    def evaluate_students(self):
        """Evaluates students accross all quizzes and returns their evaluation as dictonary with key being student id
        and value reffering to list of dictionaries with entries containing quizz_id and sucess rate."""

        student_evaluation = dict()

        for quizz in self.quizzes:
            for student in list(self.students.values()):
                success_rate = quizz.validate_answers(student.submit_answers(quizz.id))
                if student.id in student_evaluation:
                    student_evaluation[student.id].append(dict(quizz_id=quizz.id, success_rate=success_rate))
                else:
                    student_evaluation[student.id] = [dict(quizz_id=quizz.id, success_rate=success_rate)]

        return student_evaluation


    def __repr__(self):
        """Returns string representation of class."""

        return '<Class: name = "{}", teacher = {}, students = {}, quizzes = {}>'.format(self.name,
                                                                                        self.teacher,
                                                                                        self.students,
                                                                                        self.quizzes)
