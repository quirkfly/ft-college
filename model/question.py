# Model of question. Question consists of text, a set of choices and a set of correct choices.
# It provides comparison of selected choices against correct ones.

from frozendict import frozendict

class Question(object):
    def __init__(self, text, choices, correct_choices):
        self.text = text
        self.choices = frozendict(choices)
        self.correct_choices = set(correct_choices)


    def __eq__(self, other):
        """Returns True if this and other question are equal, False otherwise."""

        return self.text == other.text


    def __contains__(self, choice):
        """Returns True if question contains a choice, False otherwise."""

        return choice in self.choices


    def are_choices_correct(self, selected_choices):
        """Returns True if selected_choices and correct_choices are identical, False otherwise."""

        return set(selected_choices) == self.correct_choices


    def __repr__(self):
        """Returns string representation of question."""

        return '<Question: text= "{}", choices = {}, correct_choices = {}'.format(self.text, self.choices,
                                                                                  self.correct_choices)