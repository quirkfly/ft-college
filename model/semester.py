#  Model of semester. Encapsulates elementary semester attributes such as period and year. Knows how to compare itself
# against another semester.

from enum import Enum


class Period(Enum):
    SUMMER = 1
    WINTER = 2


class Semester(object):
    def __init__(self, period, year):
        self.period = period
        self.year = year


    @property
    def label(self):
        """Returns semester label."""

        return '{}/{}'.format(self.period, self.year)


    @property
    def number_quizzes(self):
        """Returns number of quizzes in this semester."""

        return len(self.quizzes)


    def __eq__(self, other):
        """Returns True if this and other semester equal, False otherwise."""

        return self.period == other.period and self.year == other.year


    def __repr__(self):
        """Returns semester string representation."""

        return '<Semester: period = {}, year = {}>'.format(self.period, self.year)