# Model of quizz. Each class maintains a mapping between question id and question instance.
# It implements question iterator a  coordinates answer validation.


from uuid import uuid4

from model.exception import NoSuchQuestion


class Quizz(object):
    @classmethod
    def make_id(cls):
        return str(uuid4())


    def __init__(self, id, name, questions):
        self.id = id
        self.name = name
        self.questions = { question.text: question for question in questions }
        self.question_index = 0
        self.teacher = None


    @property
    def number_questions(self):
        """Returns number of questions this quizz consists of."""

        return len(self.questions)

    # TODO: consider making iterator when student will be answering questions


    def __eq__(self, other):
        """Returns True if this and other quizz are equal, False otherwise."""

        return self.id == other.id


    def get_question(self, index):
        """Returns question at <index> (1 referring to first question) position if within range,
        raises NoSuchQuestion exception otherwise."""

        questions = list(self.questions.values())

        try:
            return questions[index - 1]
        except Exception:
            raise NoSuchQuestion


    def __iter__(self):
        """Initializes quizz iterator context and returns self."""

        self.question_index = 0

        return self


    def __next__(self):
        """Returns next question if within range, raises StopIteration otherwise."""

        if self.question_index < len(self.questions):
            question = self.get_question(self.question_index + 1)
            self.question_index += 1

            return question
        else:
            raise StopIteration


    def validate_answers(self, answers):
        """Returns number of correct answers divided by total number of questions."""

        num_correct_answers = 0

        for question_text, selected_choices in iter(answers.items()):
            question = self.questions[question_text]
            if question.are_choices_correct(selected_choices):
                num_correct_answers += 1

        return num_correct_answers / len(self.questions)


    def __repr__(self):
        """Returns string representation of quizz."""

        return '<Quizz id = "{}", name = "{}", questions = "{}", teacher = {}>'.format(self.id, self.name,
                                                                                       self.questions.values(),
                                                                                       self.teacher)