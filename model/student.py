# Model of student. Each student keeps track about his/her answers of quizz questions.
# Upon evaluation receives final grading for specific semester.

from model.exception import NoSuchQuizz
from model.person import Person


class Student(Person):
    def __init__(self, id, first_name, last_name):
        super().__init__(id, first_name, last_name)

        self.answers = dict()
        self.grades = dict()


    @property
    def number_semesters(self):
        """Returns number of semesters of this student."""

        return len(self.grades)


    def answer_question(self, quizz_id, question_text, choices):
        """Answers a quizz question,"""

        if quizz_id in self.answers:
            self.answers[quizz_id][question_text] = choices
        else:
            self.answers[quizz_id] = {question_text: choices}


    def submit_answers(self, quizz_id):
        """Here student gets a last chance to perform final corrections
        before submission. Submits answers, as dictonary with key being question text and value containing
        selected choices, for a specific quizz. Raise NoSuchQuizz exception if quizz was not found."""

        if not quizz_id in self.answers:
            raise NoSuchQuizz

        return self.answers[quizz_id]


    def set_grade(self, semester, grade):
        """Sets this student grade for specific semester."""

        self.grades[semester] = grade


    def __repr__(self):
        """Returns student string representation."""

        return '<Student: id ="{}", first_name = "{}", last_name ="{}", grades={}>'.format(self.id,
                                                                                           self.first_name,
                                                                                           self.last_name,
                                                                                           self.grades)