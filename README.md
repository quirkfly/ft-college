# Objective

To create an example that would illustrate a collaboration of model classes within an imaginary college. In general it would outlined a collection of abstractions one can encounter when dealing with the education domain.

# Design

In the center there is **class** managed by **teacher** and consisting of **students**. Students answer questions belonging to **quizz** created by teacher and assigned to class. Once answered, teacher evaluates class and assigns grades to students for specific **semester**.

# Usage

Import model classes and fixture

```python
from model.semester import Semester, Period
from model.teacher import Teacher
from model.clazz import Class
from model.student import Student
from model.quizz import Quizz
from fixture.question import QUESTION_LIST1, QUESTION_LIST2
```

Create a semester, a teacher and a class instances

```python
semester = Semester(Period.WINTER, 2018)
teacher = Teacher(Teacher.make_id(), "Louis", "Dunn")
clazz = Class('I.A', semester, teacher)
```

Create student instances and add them to the class. Add the class to the teacher

```python
student1 = Student(Student.make_id(), "John", "Sanchez")
student2 = Student(Student.make_id(), "Kayla", "Blanchette")
clazz.add_students([student1, student2])    
teacher.add_class(clazz)
```

Create quizz instances and assign them to the class.

```python
quizz1 = teacher.create_quizz(id=Quizz.make_id(), name="The Simpsons Quiz for Beginners",
                                  questions=QUESTION_LIST1)    								  
teacher.assign_quizz(quizz1, clazz.name)

quizz2 = teacher.create_quizz(id=Quizz.make_id(), name="The Simpsons Quiz for Experts",
                                  questions=QUESTION_LIST2)
teacher.assign_quizz(quizz2, clazz.name)
```

Let students to answer the quizz questions

```python
student1.answer_question(quizz_id=quizz1.id, question_text=QUESTION_LIST1[0].text, choices=['A', 'B'])
student1.answer_question(quizz_id=quizz1.id, question_text=QUESTION_LIST1[1].text, choices=['C'])
student1.answer_question(quizz_id=quizz1.id, question_text=QUESTION_LIST1[2].text, choices=['B', 'D'])
student2.answer_question(quizz_id=quizz1.id, question_text=QUESTION_LIST1[0].text, choices=['A', 'B'])
student2.answer_question(quizz_id=quizz1.id, question_text=QUESTION_LIST1[1].text, choices=['A'])

student1.answer_question(quizz_id=quizz2.id, question_text=QUESTION_LIST2[0].text, choices=['C'])
student1.answer_question(quizz_id=quizz2.id, question_text=QUESTION_LIST2[1].text, choices=['A', 'C'])
student2.answer_question(quizz_id=quizz2.id, question_text=QUESTION_LIST2[0].text, choices=['C'])
```

Ask the teacher to evalute classes.

```
teacher.evaluate_classes()
```

Print student instances

```python
for student in [student1, student2]:
	print(student)
```

Sample output

```python
<Student: id ="42863b24-41bf-40f7-8aa0-60e6fe4011a1", first_name = "John", last_name ="Sanchez", grades={'Period.WINTER/2018': 'A'}>
<Student: id ="6c32cab8-e06b-41a0-8624-c435853eef7c", first_name = "Kayla", last_name ="Blanchette", grades={'Period.WINTER/2018': 'C'}>
```

# Dependencies

Question class represents **choices** attribute as **frozendict** due to its immutable nature. Install it as follows

```bash
$ sudo pip install frozendict
```